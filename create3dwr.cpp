#include <bits/stdc++.h>
using namespace std;

int main(int args,char **argv)
{

	ofstream outfile;
    outfile.open("cfile_rec3d.dat");
    int sz = 20,fac=atoi(argv[1]);
    int nc = sz*fac;
    int nr = sz*fac;
    int nz = 4*fac;
    
    int nobs = (4*fac*fac+10*fac*fac*4+4*4*fac*fac*4)*9;
    outfile<<nc<<"\n";
    outfile<<nr<<"\n";
    outfile<<nz<<"\n";

    outfile<<nobs<<"\n";

    for(int z=0;z<3;z++)
    {
	   	int x,y,i,j;
		x = 2*fac;
		y = 2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";

		x = nc-2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";

		y = nr-2*fac;
		x = 2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x+j<<" "<<y-i<<" "<<z<<"\n";

		x = nc-2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x-j<<" "<<y-i<<" "<<z<<"\n";	
	    //////////////////////////////////////////////

		x = 2*fac;
		y = 5*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";	
		}

		x = nc-2*fac;
		y = 5*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = 2*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+i<<" "<<y+j<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = nr-2*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+i<<" "<<y-j<<" "<<z<<"\n";
		}
	    
	    ////////////////////////////////////////////////
		//Drawing square obstacles
		x = 5*fac;
		y = 5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = nr-5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x+j<<" "<<y-i<<" "<<z<<"\n";
		}


		x = nc-5*fac;
		y = 5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";
		}

		

		x = nc-5*fac;
		y = nr-5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x-j<<" "<<y-i<<" "<<z<<"\n";
		}

    }
	
	for(int z=9;z<12;z++)
    {
	   	int x,y,i,j;
		x = 2*fac;
		y = 2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";

		x = nc-2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";

		y = nr-2*fac;
		x = 2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x+j<<" "<<y-i<<" "<<z<<"\n";

		x = nc-2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x-j<<" "<<y-i<<" "<<z<<"\n";	
	    //////////////////////////////////////////////

		x = 2*fac;
		y = 5*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";	
		}

		x = nc-2*fac;
		y = 5*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = 2*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+i<<" "<<y+j<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = nr-2*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+i<<" "<<y-j<<" "<<z<<"\n";
		}
	    
	    ////////////////////////////////////////////////
		//Drawing square obstacles
		x = 5*fac;
		y = 5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = nr-5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x+j<<" "<<y-i<<" "<<z<<"\n";
		}


		x = nc-5*fac;
		y = 5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";
		}

		

		x = nc-5*fac;
		y = nr-5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x-j<<" "<<y-i<<" "<<z<<"\n";
		}

    }

    for(int z=16;z<19;z++)
    {
	   	int x,y,i,j;
		x = 2*fac;
		y = 2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";

		x = nc-2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";

		y = nr-2*fac;
		x = 2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x+j<<" "<<y-i<<" "<<z<<"\n";

		x = nc-2*fac;
		for(i=0;i<fac;i++)
			for(j=0;j<fac;j++)
				outfile<<x-j<<" "<<y-i<<" "<<z<<"\n";	
	    //////////////////////////////////////////////

		x = 2*fac;
		y = 5*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";	
		}

		x = nc-2*fac;
		y = 5*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = 2*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+i<<" "<<y+j<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = nr-2*fac;
		for(i=0;i<10*fac;i++)
		{
			for(j=0;j<fac;j++)
			outfile<<x+i<<" "<<y-j<<" "<<z<<"\n";
		}
	    
	    ////////////////////////////////////////////////
		//Drawing square obstacles
		x = 5*fac;
		y = 5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x+j<<" "<<y+i<<" "<<z<<"\n";
		}

		x = 5*fac;
		y = nr-5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x+j<<" "<<y-i<<" "<<z<<"\n";
		}


		x = nc-5*fac;
		y = 5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x-j<<" "<<y+i<<" "<<z<<"\n";
		}

		

		x = nc-5*fac;
		y = nr-5*fac;
		for(i=0;i<4*fac;i++)
		{
			for(j=0;j<4*fac;j++)
				outfile<<x-j<<" "<<y-i<<" "<<z<<"\n";
		}

    }
	

	outfile<<"5"<<"\n";

	outfile<<fac<<" "<<10*fac<<" 5 "<<"4\n";

	outfile<<(nc-fac)<<" "<<(10*fac)<<" 15 5\n";
	int x,y;
	
	y = 5*fac+5*fac;
	x = 5*fac;
	outfile<<x<<" "<<y<<" 5 1\n";
	


	y = 5*fac;
	x = 5*fac+5*fac;
	outfile<<x<<" "<<y<<" 15 2\n";


	y = nc-5*fac;
	x = nr-5*fac-5*fac;
	outfile<<x<<" "<<y<<" 5 3\n";

}