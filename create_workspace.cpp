#include <bits/stdc++.h>
using namespace std;

int main(int args,char **argv)
{

	ofstream outfile;
    outfile.open("cfile_rec.dat");
    int sz = 20,fac=atoi(argv[1]);
    int nr = sz*fac;
    int nc = sz*fac;
    
    int nobs = 4*fac*fac+10*fac*fac*4+4*4*fac*fac*4;
    outfile<<nr<<"\n";
    outfile<<nc<<"\n";

    outfile<<nobs<<"\n";

	int x,y,i,j;
	x = 2*fac;
	y = 2*fac;
	for(i=0;i<fac;i++)
		for(j=0;j<fac;j++)
			outfile<<y+i<<" "<<x+j<<"\n";

	x = nc-2*fac;
	for(i=0;i<fac;i++)
		for(j=0;j<fac;j++)
			outfile<<y+i<<" "<<x-j<<"\n";

	y = nr-2*fac;
	x = 2*fac;
	for(i=0;i<fac;i++)
		for(j=0;j<fac;j++)
			outfile<<y-i<<" "<<x+j<<"\n";

	x = nc-2*fac;
	for(i=0;i<fac;i++)
		for(j=0;j<fac;j++)
			outfile<<y-i<<" "<<x-j<<"\n";	
    //////////////////////////////////////////////

	x = 2*fac;
	y = 5*fac;
	for(i=0;i<10*fac;i++)
	{
		for(j=0;j<fac;j++)
		outfile<<y+i<<" "<<x+j<<"\n";	
	}

	x = nc-2*fac;
	y = 5*fac;
	for(i=0;i<10*fac;i++)
	{
		for(j=0;j<fac;j++)
		outfile<<y+i<<" "<<x-j<<"\n";
	}

	x = 5*fac;
	y = 2*fac;
	for(i=0;i<10*fac;i++)
	{
		for(j=0;j<fac;j++)
		outfile<<y+j<<" "<<x+i<<"\n";
	}

	x = 5*fac;
	y = nr-2*fac;
	for(i=0;i<10*fac;i++)
	{
		for(j=0;j<fac;j++)
		outfile<<y-j<<" "<<x+i<<"\n";
	}
    
    ////////////////////////////////////////////////
	//Drawing square obstacles
	x = 5*fac;
	y = 5*fac;
	for(i=0;i<4*fac;i++)
	{
		for(j=0;j<4*fac;j++)
			outfile<<y+i<<" "<<x+j<<"\n";
	}

	x = 5*fac;
	y = nr-5*fac;
	for(i=0;i<4*fac;i++)
	{
		for(j=0;j<4*fac;j++)
			//cout<<(y-i)<<(x+j)<<" ";
			outfile<<y-i<<" "<<x+j<<"\n";
	}


	x = nc-5*fac;
	y = 5*fac;
	for(i=0;i<4*fac;i++)
	{
		for(j=0;j<4*fac;j++)
			outfile<<y+i<<" "<<x-j<<"\n";
	}

	

	x = nc-5*fac;
	y = nr-5*fac;
	for(i=0;i<4*fac;i++)
	{
		for(j=0;j<4*fac;j++)
			outfile<<y-i<<" "<<x-j<<"\n";
	}

	int nprop=6*fac*fac;

	outfile<<"5"<<"\n";

	outfile<<fac<<" "<<10*fac<<" "<<"4\n";


	// x = 1;
	// y = 1;
	// for(i=0;i<fac;i++)
	// {
	// 	for(j=0;j<fac;j++)
	// 		outfile<<y+i<<" "<<x+j<<" 5\n";
	// }
	
	
	outfile<<(nc-fac)<<" "<<(10*fac)<<" 5\n";
	// for(i=0;i<fac;i++)
	// {
	// 	for(j=0;j<fac;j++)
	// 		outfile<<y-i<<" "<<x-j<<" 6\n";
	// }
	
	
	y = 5*fac+5*fac;
	x = 5*fac;
	outfile<<y<<" "<<x<<" 1\n";
	// for(i=0;i<fac;i++)
	// {
	// 	for(j=0;j<fac;j++)
	// 		outfile<<y+i<<" "<<x+j<<" 1\n";
	// }


	y = 5*fac;
	x = 5*fac+5*fac;
	outfile<<y<<" "<<x<<" 2\n";
	// for(i=0;i<fac;i++)
	// {
	// 	for(j=0;j<fac;j++)
	// 		outfile<<y+i<<" "<<x+j<<" 2\n";
	// }


	y = nc-5*fac;
	x = nr-5*fac-5*fac;
	outfile<<y<<" "<<x<<" 3\n";
	// for(i=0;i<fac;i++)
	// {
	// 	for(j=0;j<fac;j++)
	// 		outfile<<y-i<<" "<<x-j<<" 3\n";
	// }


	// x = nc-5*fac-5*fac;
	// y = nr-5*fac;
	// outfile<<y<<" "<<x<<" 4\n";
	// for(i=0;i<fac;i++)
	// {
	// 	for(j=0;j<fac;j++)
	// 		outfile<<y-i<<" "<<x-j<<" 4\n";
	// }



}